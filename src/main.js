import Vue from "vue";
import router from "./router";
import "./util/enhance";
import "./styles/global.scss";
import apis from "./api";
import store from "./store";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import App from "./App";
import service from "./server";

Vue.config.productionTip = false;
Vue.prototype.$http = service;
Vue.prototype.APIS = apis;
Vue.use(ElementUI);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
