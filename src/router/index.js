import Vue from "vue";
import VueRouter from "vue-router";
import Layout from "@/layout/index";
import { constantRoutes } from "./constantRoutes";
import { asyncRoutes } from "./asyncRoutes";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "hash",
  routes: [
    {
      path: "/login",
      name: "Login",
      component: () => import("@/views/Login"),
    },
    {
      path: "/",
      name: "Layout",
      component: Layout,
      redirect: "/giftManage",
      children: [...asyncRoutes, ...constantRoutes],
    },
    {
      path: "*",
      component: () => import("@/views/NotFound"),
    },
  ],
});

export default router;
