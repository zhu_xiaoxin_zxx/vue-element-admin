export const asyncRoutes = [
  {
    path: "/personalSettings",
    name: "PersonalSettings",
    component: () => import("@/views/PersonalSettings"),
    meta: {
      title: "人员配置",
    },
  },
];
