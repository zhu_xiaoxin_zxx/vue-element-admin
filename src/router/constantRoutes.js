export const constantRoutes = [
  {
    path: "/giftManage",
    name: "GiftManage",
    component: () => import("@/views/GiftManage"),
    meta: {
      title: "礼品管理",
    },
  },
  {
    path: "/feedback",
    name: "Feedback",
    component: () => import("@/views/Feedback"),
    meta: {
      title: "反馈中心",
    },
  },
  {
    path: "/dataTable",
    name: "DataTable",
    component: () => import("@/views/DataTable"),
    meta: {
      title: "数据报表",
    },
  },
  {
    path: "/tableDetails",
    name: "TableDetails",
    component: () => import("@/views/DataTable"),
    meta: {
      title: "报表详情",
      hidden: true,
    },
  },
];
