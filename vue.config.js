module.exports = {
  publicPath: '',
  filenameHashing: true,
  productionSourceMap: false,
  devServer: {
    open: true,
    port: 8080,
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        ws: true
      }
    },
  }
};